#!/usr/bin/env python3

import sys

try:
    import random, datetime, uuid, time, rediscluster, redis
except ModuleNotFoundError as err:
    print('\n%s' % err)
    print ('\nAre you in a proper Python virtual environment? And did you')
    print ('run \'pip install -r requirements.txt\' ?\n')
    sys.exit(1)

if len(sys.argv) < 3:
    print ()
    print ("Usage:   populate_redis.py NUM_ROWS NUM_COLS ENDPOINT")
    print ()
    sys.exit(1)

## SETUP GLOBAL VALUES
NUM_ROWS = int(sys.argv[1])
NUM_COLS = int(sys.argv[2])
ENDPOINT = sys.argv[3]

ALL_SORTED_SETS = {}

# COMMENT OUT FOR NON-CLUSTER MODE
"""
from rediscluster.client import RedisCluster
startup_nodes= [{'host': ENDPOINT, 'port':6379}]
pool = rediscluster.ClusterBlockingConnectionPool(
    startup_nodes=startup_nodes,
    decode_responses=True,
    skip_full_coverage_check=True)
r = RedisCluster(connection_pool=pool)
"""

# COMMENT OUT FOR CLUSTER MODE

from redis import Redis
pool = redis.BlockingConnectionPool(
    host=ENDPOINT,
    decode_responses=True)
r = redis.Redis(connection_pool=pool)

pipe = r.pipeline(transaction=False)

def get_date_str(days_ago):

    target_date = str(datetime.date.today() - datetime.timedelta(days=days_ago))
    return target_date.replace('-','')


def get_ms():

    return round(time.time() * 1000)


def generate_data():

    print ('===== NEW RUN =====')
    print ('Generating data...', flush=True)

    start_time = get_ms()
    for row in range (NUM_ROWS):
        key_name = str(uuid.uuid1())
        sorted_set = {}
        # Generate one date entry (column) per day and add to sorted set
        for col in reversed(range (NUM_COLS)):
            value = str(round(random.uniform(10,5000000000), 8))
            score = str(get_date_str(col))
            sorted_set[value] = score
        ALL_SORTED_SETS[key_name] = sorted_set
    delta = get_ms() - start_time
    print ('Generated data in %s milliseonds' % delta)

def populate_sorted_sets():

    print ('Populating Redis sorted sets...', flush=True)
    start_time = get_ms()
    
    for key, value in ALL_SORTED_SETS.items():
        pipe.zadd(key, value)
    
    pipe.execute()
    delta = get_ms() - start_time
    print ('Created %s Redis sorted sets in %s milliseconds' % (NUM_ROWS, delta))


def main():

    generate_data()
    populate_sorted_sets()


if __name__ == "__main__":
    
    main()
