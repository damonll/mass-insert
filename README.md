# Mass Insert with Dates

Created as an example for a customer to demonstrate mass insertion performance.

## Background

In this example, there is one Redis sorted set per customer (represented with a UUID). For each entry, the Redis score is the numeric value of the datestamp (i.e. `20121225` for Dec 25, 2012) and the Redis value is a float value to represent financial information about the customer.

All dates (default 365 for one year) and values for a customer are added to an internal Python object. Once the full object is built, we send that object to Redis via `zadd` so we can avoid hundreds or thousands of `zadd` calls per Redis sorted set.

This example uses connection pooling and Redis pipelines, though connection pooling does not have a meaningful impact in this use case as we utilize one connection per Python instance and avoid opening / closing multiple connections.

## Performance

This setup can be run from a single client system or multiple client systems. Initial testing with a single 16-core system (c5.4xlarge) allowed for 12 clients to run simultaneously (one Python instance per core). Running against a 4-node m6g.xlarge-based ElastiCache for Redis cluster, this setup achieved approximately 4.7GB of data being inserted in 28 seconds. Further optimizations could be made to incrementally improve performance.

## Step 1 - Setup Python

Requires Python 3. Create a Python virtual environment if you haven't already:

```console
python3 -m venv venv
```

Then activate it:

```console
source venv/bin/activate
```

Now install the necessary libraries:

```console
pip install -r requirements.txt
```

## Step 2 - Populate Redis

Find your ElastiCache or MemoryDB configuration endpoint, this will be needed.

Run the `start` bash shell script which will in turn run multiple Python processes.

**NOTE** Be sure to remove data after each run so Redis is not taking more memory than necessary. Use the `FLUSHDB` or `FLUSHALL` command on each primary node (if you have more than one).

## Step 3 - Test in non-cluster mode (optional)

You can modify the `populate_redis.py` script and comment out the cluster
section, and uncomment the non-cluster section. This will allow testing
against a non-clustered Redis process (typically a locally installed Redis on
your laptop).

The `start` shell script can still be used in this configuration.
